# Read Me First
this is an API for school management 
that does the CRUD ops on Entities

it will fully support crud operations 

these are post examples:
# Getting Started
### 1.first you add a college like :

###### POST http://localhost:8080/api/college

###### Content-Type: application/json

###### {

###### "collegeName": "Computer"

###### }
### 2.then add a professor like

###### POST http://localhost:8080/api/professor?collegeId=1

###### Content-Type: application/json

###### {

###### "firstName": "Behnam",

###### "lastName": "Si",

###### "personalId": 9711111,

###### "nationalId": 4581111111

}

### 3.then add a course:

###### POST http://localhost:8080/api/course?professorId=1&collegeId=1

###### Content-Type: application/json

###### {

###### "courseName": "Java",

###### "unitNumber": 3

###### }

### 4.then add a student:

###### POST http://localhost:8080/api/student?collegeId=1

###### Content-Type: application/json

###### {

###### "firstName": "Sepehr",

###### "lastName": "Si",

###### "nationalId": 4582222222,

###### "universityId": 9922222

###### }

# NOTE

### configurations for database is on create-drop 

* its for debug for keeping the data use
* #### "UPDATE"

### author : Behnam Si